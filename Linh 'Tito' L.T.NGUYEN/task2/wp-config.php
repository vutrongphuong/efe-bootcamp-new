<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'task2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ko&?48`-]Ns;o;Uy*kEqN@jxN->N>7k`P)+TQoh!wy, 0k3mT:EUKmgP**Te};d;');
define('SECURE_AUTH_KEY',  '&MSSpUSu2ixu]| BeOjGUFylB7$K;:YLf%-l6~ ]Lb*j~G[-x~ap^oAT|J!T$PTF');
define('LOGGED_IN_KEY',    'R,XRGda,C1dshE%Sp#pg)>HbZ|SBq-Nv:z)ICGN1*O9>q@/N@{@Q8:z.C}f%%pw9');
define('NONCE_KEY',        'R)!BQ^[fc=B^myQP0m;4T/bHF.[hap%Q)Z:}ZyG05m?bF,8n(/k: J&c)$zou+oo');
define('AUTH_SALT',        'vDpP6)PF#85&QRH&)^6:|o#SZd~|RE(l.a3m3J?DD=[hS^8x( 2Y-H{`6>hS0%G<');
define('SECURE_AUTH_SALT', '$q-bjTz!YJd3(XQ:a(.~FygE0VE<,hGf3].z!=,LL{qGSV}0AH7miN8cuU7D>#`o');
define('LOGGED_IN_SALT',   'SJ3lBnTM!<Mwx?_|0`IMLDGotM|un4S!ujG+|@3U&8SPor{Q=e9rx=XA8rUJOb&5');
define('NONCE_SALT',       'rb48U9Yu{w*Z3KXqTl6kS:]fGpLl`H)x)c7L;ei@,Zxt;!!T|KZIMi DvQrv=f|e');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
