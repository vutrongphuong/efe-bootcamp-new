<?php
/**
 * Plugin Name: WPTQ Page Settings
 * Plugin URI: 
 * Description: Thêm page settings cho website của bạn
 * Version: 1.0.0
 * Author: TruongTuyen
 * Author URI: 
 */
   
//SETTINGS PAGE add menu for admin page
$wptq_settings = '';
add_action( 'admin_menu', 'wptq_create_admin_menu' );
function wptq_create_admin_menu(){
    add_menu_page( 'SharpSpring Integration', 'SharpSpring Integration', 'manage_options', 'wptq_settings_page', 'wptq_settings_page_content', "dashicons-align-right" );
}
function wptq_settings_page_content(){
    $all_settings = get_option( 'wptq_settings' );
    global $wptq_settings;
    $wptq_settings = $all_settings;
    ?>
    <div class="wrap">
        <h1>WordPress SharpSpring Integration</h1>
        
        <form method="post" action="options.php">
            <?php
            settings_fields( 'wptq_settings_general' );
            do_settings_sections( 'wptq_settings_page' );
            ?>
            
            <?php submit_button(); ?>
        
        </form>
        </div>
   <?php
}
//end SETTINGS PAGE add menu for admin page




add_action( 'admin_init', 'wptq_page_setting_init' );
function wptq_page_setting_init(){
    register_setting(
        'wptq_settings_group',
        'wptq_settings'
    );
    
    //GENERAl SECTION Bắt buộc
    add_settings_section(
        'wptq_settings_general', // ID
        'General Settings', // Title
        'wptq_general_settings', // Callback
        'wptq_settings_page' // Page

    );
    
    //móc function wptq_text_field_content()
    add_settings_field(
        'text_field', // ID
        'Text Field', // Title 
        'wptq_text_field_content', // Callback
        'wptq_settings_page', // Page
        'wptq_settings_general' // Section           
    );


    //móc function wptq_text_field_checkbox()
    add_settings_field(
        'text_field1', // ID
        'Text Field', // Title 
        'wptq_text_field_checkbox', // Callback
        'wptq_settings_page', // Page
        'wptq_settings_general' // Section           
    ); 

    
    // OTHER SECTION
   
    register_setting( 'wptq_settings_general', 'textarea' );
    register_setting( 'wptq_settings_general', 'check' );
    
 
}
function wptq_text_field_checkbox() {
    ?>
        <input type="checkbox" name="check">
    <?php
}
function wptq_general_settings(){
    echo "Config General Settings";
}

function wptq_text_field_content(){
    global $wptq_settings;
    ?>
    <textarea name="textarea" id="" cols="70" rows="10"></textarea>
    <?php
}
function wptq_other_settings(){
    echo "Config Other Settings";
}

function add_code(){
    if(get_option('check')){
        echo get_option('textarea');
    }
}
add_action('wp_footer','add_code');
