<?php
/**
 * Plugin Name: SharpSpring Integration 
 * Plugin URI: 
 * Description: This is the Plugin for admin, add content for Footer.php
 * Version: 1.0.0
 * Author: ThanhLinh
 * Author URI: 
 */


//SETTINGS PAGE
$wptq_settings = '';
add_action( 'admin_menu', 'wpsi_create_admin_menu' );
function wpsi_create_admin_menu(){
    add_menu_page( 'Sharpspring integration Settings', 'Sharpspring integration Plugin', 'manage_options', 'setting_page', 'settings_page_content');
}



function settings_page_content(){
    $all_settings = get_option( 'wpsi_settings' );
    global $wpsi_settings;
    $wpsi_settings = $all_settings;
    ?>
    <div class="wrap">
        <h1>WordPress SharpSpring Integration</h1>
        
        <form method="post" action="options.php">
            <?php
            settings_fields( 'wpsi_settings_group' );
            do_settings_sections( 'setting_page' );
            ?>
            
            <?php submit_button(); ?>
        
        </form>
        </div>
   <?php
}


add_action( 'admin_init', 'wpsi_page_setting_init' );

function wpsi_page_setting_init()
{
		// register_setting(
	 //        'wpsi_settings_group',
	 //        'wpsi_settings'
	 //    );

	    register_setting('wpsi_settings_group','Sharp_Check');
   		register_setting('wpsi_settings_group','Sharp_TextArea');

   		add_settings_field(
	       'trackingcode', // ID
	       'Tracking code', // Title
	       'tracking_code', // Callback
	       'setting_page', // Page
	       'Sharp_Section' // Section
	   );

   		add_settings_field(
	       'active_tracking', // ID
	       'Active', // Title
	       'sharp_active', // Callback
	       'setting_page', // Page
	       'Sharp_Section' // Section
	   );


   		add_settings_section(
	       'Sharp_Section', // ID
	       '', // Title
	       '', // Callback
	       'setting_page' // Page
	   );	      
}


function tracking_code()
{
   ?>
   <textarea class="textarea" style="width: 600px; height: 400px;"  name="Sharp_TextArea"><?php echo get_option('Sharp_TextArea'); ?></textarea>
   <?php
}

function sharp_active()
{

   ?>
   <input type="checkbox" name="Sharp_Check" id="active_check" <?php echo(get_option('Sharp_Check')?'checked':'') ?>>
   <?php
}


add_action('wp_footer','create_footer_form');
function create_footer_form()
{
   if(get_option('Sharp_Check'))
   {
       echo get_option('Sharp_TextArea');
   }
}